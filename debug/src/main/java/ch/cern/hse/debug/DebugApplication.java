package ch.cern.hse.debug;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.HeaderHttpSessionIdResolver;
import org.springframework.session.web.http.HttpSessionIdResolver;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@SpringBootApplication
@EnableWebSecurity
@EnableRedisHttpSession
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RestController
public class DebugApplication extends WebSecurityConfigurerAdapter {

    public static void main(String[] args) {
        SpringApplication.run(DebugApplication.class, args);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/actuator/**").permitAll()
                .anyRequest().fullyAuthenticated()

                .and().addFilterBefore(new SecurityContextPersistenceFilter(), RequestHeaderAuthenticationFilter.class);

    }
//
//    @Bean
//    public HttpSessionIdResolver httpSessionStrategy() {      // Header Strategy indicate that session will be manage using header
//        return HeaderHttpSessionIdResolver.xAuthToken();
//    }

    @Bean
    public LettuceConnectionFactory connectionFactory(RedisProperties redisProperties) {
        return new LettuceConnectionFactory(redisProperties.getHost(), redisProperties.getPort());
    }

    @GetMapping("/")
    public String hello(HttpSession session) {

        return "SESSION: " + session.getId() + "\nRoles: " + session.getAttribute("roles");

    }

    @GetMapping("/secure")
    @PreAuthorize("hasRole('ADMIN')")
    public String helloSecure(HttpSession session) {

        return "SECURED SESSION: " + session.getId() + "\nRoles: " + session.getAttribute("roles");

    }


    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }
}
