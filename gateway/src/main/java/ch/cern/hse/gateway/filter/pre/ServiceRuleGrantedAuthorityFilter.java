package ch.cern.hse.gateway.filter.pre;

import ch.cern.hse.gateway.ZuulRoutingRulesProperties;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpStatus;
import org.springframework.session.Session;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@Slf4j
@Component
public class ServiceRuleGrantedAuthorityFilter extends ZuulFilter {


    @Autowired
    private RedisOperationsSessionRepository repository;

    private ZuulRoutingRulesProperties rulesProperties;

    public ServiceRuleGrantedAuthorityFilter(ZuulRoutingRulesProperties properties) {
        super();
        this.rulesProperties = properties;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 6;
    }

    @Override
    public boolean shouldFilter() {
        return !RequestContext.getCurrentContext().getRequest().isUserInRole("ROLE_ADMIN");
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        HttpSession httpSession = request.getSession(false);
        Session session = repository.findById(httpSession.getId());

        final boolean debug = log.isDebugEnabled();
        final String serviceId = (String) ctx.get(FilterConstants.SERVICE_ID_KEY);

        boolean filter = session == null;
        final String user = Optional.ofNullable(request.getRemoteUser()).orElse(request.getHeader("x-remote-user"));

        if (!filter) {
            if (debug) log.debug("{} - route {} - filtering based on cern.zuul properties", user, serviceId);

            ctx.addZuulRequestHeader("Cookie", "SESSION=" + session.getId());

            ZuulRoutingRulesProperties.ServiceRules thisRouteServiceRules = rulesProperties.getCredentials().get(serviceId);
            final boolean blacklist = thisRouteServiceRules.isBlacklist();

            boolean hasARoleListed = thisRouteServiceRules.getRoles().stream().anyMatch(RequestContext.getCurrentContext().getRequest()::isUserInRole);

            filter = ((blacklist && !hasARoleListed) || (!blacklist && hasARoleListed));
            // blacklist mode: access denied for any user in role listed in rules
            // whitelist mode: access allowed only if user has at least one role listed in rules
        } else {
            //could not find session in redis
            httpSession.invalidate();
            log.error("could not find session in REDIS repo which should not happen");
        }

        if (filter) {
            log.warn("{} - route {} - unauthorized access", user, serviceId);
            ctx.unset();
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
        } else if (debug) {
            log.debug("{} - route {} - authorized access", user, serviceId);
        }
        return null;
    }

}
