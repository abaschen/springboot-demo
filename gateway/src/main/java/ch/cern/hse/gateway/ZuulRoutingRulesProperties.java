package ch.cern.hse.gateway;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

@ConfigurationProperties("cern.gateway")
@Getter
@Setter
public class ZuulRoutingRulesProperties {

    /**
     * Map of ROLE to their egroups
     */
    LinkedHashMap<String, Set<String>> egroups = new LinkedHashMap<>();

    /**
     * Map of service names to their rules
     */
    LinkedHashMap<String, ServiceRules> credentials = new LinkedHashMap<>();

    @Getter
    @Setter
    @ToString
    public static class ServiceRules {
        /*
         admin-server:
            roles:
              - ADMIN
              - USER
         dosimetry-service:
            allowAny: true
        */
        /**
         * if set to true, rules will be in blacklist mode (permit all and filter some)
         * Default is false which mean rules are in whitelist mode (deny all and allow some)
         */
        boolean blacklist = false;

        /**
         * Set of roles to allow or deny access
         */
        LinkedHashSet<String> roles = new LinkedHashSet<>();
    }
}
