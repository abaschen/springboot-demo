package ch.cern.hse.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@EnableDiscoveryClient
@EnableZuulProxy
@EnableConfigurationProperties({ZuulRoutingRulesProperties.class})
@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args) {
        final SpringApplication app = new SpringApplication(GatewayApplication.class);
        app.setWebApplicationType(WebApplicationType.SERVLET);
        app.run(args);
    }

    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }
}
