package ch.cern.hse.gateway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class HeaderBasedAuthenticationUserDetailsService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

    private ZuulRoutingRulesProperties rulesProperties;

    public HeaderBasedAuthenticationUserDetailsService(ZuulRoutingRulesProperties rulesProperties) {
        this.rulesProperties = rulesProperties;
    }

    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken authentication) throws UsernameNotFoundException {
        final boolean debug = log.isDebugEnabled();
        final String groups = (String) authentication.getCredentials();
        final String login = (String) authentication.getPrincipal();

        if (debug)
            log.debug("{} - Mapping roles", login);
        try {
            final String[] userEgroups = groups.toUpperCase().split(";");
            final LinkedHashMap<String, Set<String>> rulesEgroups = rulesProperties.getEgroups();
            List<GrantedAuthority> authorities = rulesEgroups.entrySet().stream()
                    .map(e -> new LinkedHashMap.SimpleImmutableEntry<>(e.getKey(), e.getValue()
                            .stream().filter(Objects::nonNull).map(String::trim).map(String::toUpperCase).collect(Collectors.joining(";"))))
                    .filter(authority -> Arrays.stream(userEgroups).anyMatch(authority.getValue()::contains))
                    .map(AbstractMap.SimpleImmutableEntry::getKey)
                    .map(s -> "ROLE_" + s)
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
            if (authorities.isEmpty())
                throw new UsernameNotFoundException("Invalid credentials '" + groups + "', check eGroups");

            if (debug) {
                log.debug("{} - authorized access with authorities {}", login, authorities);
            }
            return new User(login, "", authorities);

        } catch (BadCredentialsException e) {
            throw e;
        } catch (Exception e) {
            log.error("{} - unauthorized access for egroups {}, unexpected exception: {}", login, groups, e.getMessage());
            throw new UsernameNotFoundException(e.getMessage());
        }
    }
}

